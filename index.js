const HdWallet = require('eth-hd-wallet').EthHdWallet;
const readFileSync = require('fs').readFileSync;

const versionLock = 0.1;

function generateAddresses(
    mnemonicPath = 'mnemonic.json',
    count = 10,
) {
    const file = readFileSync(mnemonicPath, 'utf8');
    const { version, mnemonic } = JSON.parse(file.toString());

    if (version < versionLock) {
        throw new Error('Incompatible version of file');
    }

    const wallet = HdWallet.fromMnemonic(mnemonic);
    wallet.generateAddresses(count);

    const addresses = wallet.getAddresses();
    console.log(`Generated: ${count} addresses: \n ${addresses}`);

    return addresses;
}

function runCmd() {
    const commandArgs = process.argv.slice(2, process.argv.length);
    console.log(HdWallet);

    if (commandArgs.length === 1) {
        generateAddresses(commandArgs[0]);

        return;
    }

    if (commandArgs.length > 1) {
        generateAddresses(...commandArgs);

        return;
    }

    throw new Error('Must provide path to mnemonic file as first argument; second arg optional: count of addresses');
}

runCmd();